<?php

    require('animal.php');
    require('Frog.php');
    require('Ape.php');

    $sheep = new Animal("shaun");

    echo 'Nama Hewan : ' . $sheep->name . '<br>' ; // "shaun"
    echo 'Mempunyai Kaki: ' . $sheep->legs . '<br>' ; // 2
    echo 'Berdarah Dingin: ' . $sheep->cold_blooded . '<br><br>' ; // false

    $sungokong = new Ape("kera sakti");

    echo 'Nama Hewan: ' . $sungokong->name . '<br>';
    echo 'Mempunyai Kaki: ' . $sungokong->legs . '<br>' ; 
    echo 'Berdarah Dingin: ' . $sheep->cold_blooded . '<br>' ; 
    $sungokong->yell(); // "Auooo"
    echo '<br><br>';

    $kodok = new Frog("buduk");

    echo 'Nama Hewan: ' . $kodok->name . '<br>';
    echo 'Mempunyai Kaki: ' . $kodok->legs . '<br>' ; 
    echo 'Berdarah Dingin: ' . $kodok->cold_blooded . '<br>' ; 
    $kodok->jump() ; // "hop hop"




?>